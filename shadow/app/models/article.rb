class Article < ActiveRecord::Base
     has_many :comments
    
    validates_presence_of :title
    validates_presence_of :body
    belongs_to :writer
    
    validates_presence_of :title
    validates_length_of :title, :in => 5..30, :message => "longitud invalida"
    validates_presence_of :body
    validates :body, length: { maximum: 500, message: "El texto del post es muy largo"}
    validates :body, length: { minimum: 10, message: "el texto del post es corto"}
    
     has_attached_file :image
    
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    
    has_attached_file :csvdata,
    :url => "/controllers/articles/:basename.:extension"
    
    
end
